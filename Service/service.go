package service

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/gogo/protobuf/types"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/ShoppingCart"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

//CreateShoppingCart create new ShoppingCart
func CreateShoppingCart(entityID, businessID uuid.UUID, in *module.CreateShoppingCartRequest) (*entity.ShoppingCart, error) {
	shoppingCart := &entity.ShoppingCart{
		EntityID:   entityID,
		BusinessID: businessID,
	}
	if in.GetShippingAddress() != nil {
		address := in.GetShippingAddress()
		addressID, err := uuid.FromString(address.GetAddressID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid ShippingAddressID: %s", address.GetAddressID()))
		}
		shoppingCart.ShippingAddress = &entity.Address{
			AddressID:    addressID,
			Prefix:       address.GetPrefix(),
			Firstname:    address.GetFirstname(),
			Lastname:     address.GetLastname(),
			BusinessName: address.GetBusinessName(),
			IsBusiness:   address.GetIsBusiness(),
			Address1:     address.GetAddress1(),
			Address2:     address.GetAddress2(),
			Zip:          address.GetZip(),
			PhoneNumber:  address.GetPhoneNumber(),
			City:         address.GetCity(),
			State:        address.GetState(),
			Tax:          address.GetTax(),
		}
	}
	if in.GetBillingAddress() != nil {
		address := in.GetBillingAddress()
		addressID, err := uuid.FromString(address.GetAddressID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid BillingAddressID: %s", address.GetAddress2()))
		}
		shoppingCart.BillingAddress = &entity.Address{
			AddressID:    addressID,
			Prefix:       address.GetPrefix(),
			Firstname:    address.GetFirstname(),
			Lastname:     address.GetLastname(),
			BusinessName: address.GetBusinessName(),
			IsBusiness:   address.GetIsBusiness(),
			Address1:     address.GetAddress1(),
			Address2:     address.GetAddress2(),
			Zip:          address.GetZip(),
			PhoneNumber:  address.GetPhoneNumber(),
			City:         address.GetCity(),
			State:        address.GetState(),
			Tax:          address.GetTax(),
		}
	}
	if in.GetProduct() != nil {
		product := in.GetProduct()
		pid, err := uuid.FromString(product.GetProductID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid ProductID: %s", product.GetProductID()))
		}
		shoppingCart.ShoppingCartItems = append(shoppingCart.ShoppingCartItems, &entity.ShoppingCartItem{
			ProductID: pid,
			Quantity:  product.GetQuantity(),
		})
	}
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to create new ShoppingCart")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//GetShoppingCart by entityID, businessID
func GetShoppingCart(entityID, businessID uuid.UUID) (*entity.ShoppingCart, error) {
	return getShoppingCartByEntityAndBusiness(entityID, businessID)
}

//GetShoppingCartByID by ID
func GetShoppingCartByID(id uuid.UUID) (*entity.ShoppingCart, error) {
	return getShoppingCartByID(id)
}

//AddProductInShoppingCart add product
func AddProductInShoppingCart(id, productID uuid.UUID, amount uint64) (*entity.ShoppingCart, error) {
	if amount == 0 {
		return RemoveProductInShoppingCart(id, productID)
	}
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	alreadyInCart := false
	for _, p := range shoppingCart.ShoppingCartItems {
		if p.ProductID == productID {
			p.Quantity += amount
			alreadyInCart = true
		}
	}

	if !alreadyInCart {
		shoppingCart.ShoppingCartItems = append(shoppingCart.ShoppingCartItems, &entity.ShoppingCartItem{
			ProductID: productID,
			Quantity:  amount,
		})
	}

	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to add ProductID: %s in ShoppingCartID: %s", productID.String(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//RemoveProductInShoppingCart add product
func RemoveProductInShoppingCart(id, productID uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	index := -1
	for i, p := range shoppingCart.ShoppingCartItems {
		if p.ProductID == productID {
			index = i
		}
	}
	if index == -1 {
		return nil, status.Error(codes.NotFound, "")
	}
	if len(shoppingCart.ShoppingCartItems) > 1 {
		shoppingCart.ShoppingCartItems[index] = shoppingCart.ShoppingCartItems[len(shoppingCart.ShoppingCartItems)-1]
		shoppingCart.ShoppingCartItems = shoppingCart.ShoppingCartItems[:len(shoppingCart.ShoppingCartItems)-1]
	} else {
		shoppingCart.ShoppingCartItems = []*entity.ShoppingCartItem{}
	}
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to remove ProductID: %s in ShoppingCartID: %s", productID.String(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//AddPromoCodeInShoppingCart add promotion code to shopping cart
func AddPromoCodeInShoppingCart(id, promoCodeID uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	for _, p := range shoppingCart.PromotionCodes {
		if p.ID == promoCodeID {
			return nil, status.Error(codes.AlreadyExists, "")
		}
	}
	promo, err := getPromotionCodeByID(promoCodeID)
	if err != nil {
		return nil, err
	}

	shoppingCart.PromotionCodes = append(shoppingCart.PromotionCodes, promo)
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to add PromoCodeID: %s in ShoppingCartID: %s", promoCodeID.String(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//ValidatePromoCodesInShoppingCart validate all promo codes in cart
func ValidatePromoCodesInShoppingCart(id uuid.UUID) (*entity.ShoppingCart, []string, error) {
	var errs []string
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, errs, err
	}
	shoppingCart, errs = checkPromoCodes(shoppingCart)
	if len(errs) > 0 {
		if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
			return nil, errs, status.Error(codes.Internal, fmt.Sprintf("Failed to update ShoppingCart ID: %s", shoppingCart.ID.String()))
		}
		logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	}
	return shoppingCart, errs, nil
}

//RemovePromoCodeInShoppingCart remove promo code in shopping cart
func RemovePromoCodeInShoppingCart(id, promoCodeID uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	index := -1
	for i, p := range shoppingCart.PromotionCodes {
		if p.ID == promoCodeID {
			index = i
		}
	}
	if index == -1 {
		return nil, status.Error(codes.NotFound, "")
	}
	if len(shoppingCart.PromotionCodes) > 1 {
		shoppingCart.PromotionCodes[index] = shoppingCart.PromotionCodes[len(shoppingCart.PromotionCodes)-1]
		shoppingCart.PromotionCodes = shoppingCart.PromotionCodes[:len(shoppingCart.PromotionCodes)-1]
	} else {
		shoppingCart.PromotionCodes = []*entity.PromotionCode{}
	}
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to remove PromoCodeID: %s in ShoppingCartID: %s", promoCodeID.String(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//UpdateShippingAddressInShoppingCart update shipping address
func UpdateShippingAddressInShoppingCart(id uuid.UUID, in *module.UpdateAddressInShoppingCartRequest) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	addressID, err := uuid.FromString(in.GetAddressID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid ShippingAddressID: %s", in.GetAddressID()))
	}
	shoppingCart.ShippingAddress = &entity.Address{
		AddressID:    addressID,
		Prefix:       in.GetPrefix(),
		Firstname:    in.GetFirstname(),
		Lastname:     in.GetLastname(),
		BusinessName: in.GetBusinessName(),
		IsBusiness:   in.GetIsBusiness(),
		Address1:     in.GetAddress1(),
		Address2:     in.GetAddress2(),
		Zip:          in.GetZip(),
		PhoneNumber:  in.GetPhoneNumber(),
		City:         in.GetCity(),
		State:        in.GetState(),
		Tax:          in.GetTax(),
	}
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update ShippingAddressID: %s in ShoppingCartID: %s", in.GetAddressID(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//UpdateBillingAddressInShoppingCart update billing address
func UpdateBillingAddressInShoppingCart(id uuid.UUID, in *module.UpdateAddressInShoppingCartRequest) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	addressID, err := uuid.FromString(in.GetAddressID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Invalid BillingAddressID: %s", in.GetAddressID()))
	}
	shoppingCart.BillingAddress = &entity.Address{
		AddressID:    addressID,
		Prefix:       in.GetPrefix(),
		Firstname:    in.GetFirstname(),
		Lastname:     in.GetLastname(),
		BusinessName: in.GetBusinessName(),
		IsBusiness:   in.GetIsBusiness(),
		Address1:     in.GetAddress1(),
		Address2:     in.GetAddress2(),
		Zip:          in.GetZip(),
		PhoneNumber:  in.GetPhoneNumber(),
		City:         in.GetCity(),
		State:        in.GetState(),
		Tax:          in.GetTax(),
	}
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update BillingAddressID: %s in ShoppingCartID: %s", in.GetAddressID(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//UpdateShippingMethodInShoppingCart update shipping method ID. TODO: This need to be object later
func UpdateShippingMethodInShoppingCart(id, shippingMethodID uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	shoppingCart.ShippingMethodID = shippingMethodID
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update ShippingMethodID: %s in ShoppingCartID: %s", shippingMethodID.String(), id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//UpdatePointsRedeemInShoppingCart update points redeem
func UpdatePointsRedeemInShoppingCart(id uuid.UUID, amount uint64) (*entity.ShoppingCart, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return nil, err
	}
	shoppingCart.PointsRedeem = amount
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update points redeem in ShoppingCartID: %s", id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return shoppingCart, nil
}

//CompleteShoppingCart complete shopping cart. This is after order is placed
func CompleteShoppingCart(id uuid.UUID) (bool, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return false, err
	}
	shoppingCart.IsCompleted = true
	if err := common.DB.Save(shoppingCart).Scan(&shoppingCart).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, fmt.Sprintf("Failed to complete ShoppingCartID: %s", id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return true, nil
}

//MergeShoppingCart merge shopping cart
func MergeShoppingCart(fromID, toID uuid.UUID) (*entity.ShoppingCart, error) {
	from, err := getShoppingCartByID(fromID)
	if err != nil {
		return nil, err
	}
	if fromID == toID {
		return from, nil
	}
	to, err := getShoppingCartByID(toID)
	if err != nil {
		return nil, err
	}
	for _, pf := range from.ShoppingCartItems {
		found := false
		for _, pt := range to.ShoppingCartItems {
			if pt.ProductID == pf.ProductID {
				found = true
				break
			}
		}

		if !found {
			to.ShoppingCartItems = append(to.ShoppingCartItems, pf)
		}
	}

	for _, pf := range from.PromotionCodes {
		found := false
		for _, pt := range to.PromotionCodes {
			if pt.ID == pf.ID {
				found = true
				break
			}
		}
		if !found {
			to.PromotionCodes = append(to.PromotionCodes, pf)
		}
	}

	to, _ = checkPromoCodes(to)

	if err := common.DB.Save(to).Scan(&to).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to merge to ShoppingCartID: %s", toID.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, to.ID.String()))
	if err := common.DB.Delete(from).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to delete ShoppingCartID: %s", fromID.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, from.ID.String()))
	return to, nil
}

//DeleteShoppingCart by ID
func DeleteShoppingCart(id uuid.UUID) (bool, error) {
	shoppingCart, err := getShoppingCartByID(id)
	if err != nil {
		return false, status.Error(codes.NotFound, "")
	}
	if err := common.DB.Delete(shoppingCart).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, fmt.Sprintf("Failed to delete ShoppingCartID: %s", id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ShoppingCartChange, shoppingCart.ID.String()))
	return true, nil
}

//CreatePromoCode create promotion code
func CreatePromoCode(entityID uuid.UUID, in *module.CreatePromoCodeRequest) (*entity.PromotionCode, error) {
	from, err := types.TimestampFromProto(in.GetActiveFrom())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Invalid ActiveFrom")
	}
	to, err := types.TimestampFromProto(in.GetActiveTo())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Invalid ActiveTo")
	}
	promoCode := &entity.PromotionCode{
		EntityID:           entityID,
		Code:               strings.Trim(in.GetCode(), " "),
		IsUSD:              in.GetIsUSD(),
		IsPercent:          in.GetIsPercent(),
		DiscountAmount:     in.GetDiscountAmount(),
		LimitedAmount:      in.LimitedAmount,
		IsActive:           in.GetIsActive(),
		IsNewCustomerOnly:  in.GetIsNewCustomerOnly(),
		IsBusinessSpecific: in.GetIsBusinessSpecific(),
		ActiveFrom:         from,
		ActiveTo:           to,
	}
	if in.GetBusinessID() != "" && in.IsBusinessSpecific {
		businessID, err := uuid.FromString(in.GetBusinessID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid BusinessID")
		}
		promoCode.BusinessID = businessID
	}
	if err := common.DB.Save(promoCode).Scan(&promoCode).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to create PromotionCode %s", in.GetCode()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PromotionCodeChange, promoCode.ID.String()))
	return promoCode, nil
}

//GetAllPromoCodesByEntity by entityID
func GetAllPromoCodesByEntity(entityID uuid.UUID) ([]*entity.PromotionCode, error) {
	return getPromotionCodesByEntityID(entityID)
}

//GetPromoCodeByID by ID
func GetPromoCodeByID(id uuid.UUID) (*entity.PromotionCode, error) {
	return getPromotionCodeByID(id)
}

//GetPromoCodeByCode by entityID, code
func GetPromoCodeByCode(entityID uuid.UUID, code string) (*entity.PromotionCode, error) {
	return getPromotionCodeByCode(entityID, code)
}

//UpdatePromoCode update promo code
func UpdatePromoCode(id uuid.UUID, in *module.UpdatePromoCodeRequest) (*entity.PromotionCode, error) {
	promoCode, err := getPromotionCodeByID(id)
	if err != nil {
		return nil, err
	}
	from, err := types.TimestampFromProto(in.GetActiveFrom())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Invalid ActiveFrom")
	}
	to, err := types.TimestampFromProto(in.GetActiveTo())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Invalid ActiveTo")
	}
	promoCode.Code = strings.Trim(in.GetCode(), " ")
	promoCode.IsUSD = in.GetIsUSD()
	promoCode.IsPercent = in.GetIsPercent()
	promoCode.DiscountAmount = in.GetDiscountAmount()
	promoCode.LimitedAmount = in.LimitedAmount
	promoCode.IsActive = in.GetIsActive()
	promoCode.IsNewCustomerOnly = in.GetIsNewCustomerOnly()
	promoCode.IsBusinessSpecific = in.GetIsBusinessSpecific()
	promoCode.ActiveFrom = from
	promoCode.ActiveTo = to
	if in.GetBusinessID() != "" && in.IsBusinessSpecific {
		businessID, err := uuid.FromString(in.GetBusinessID())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "Invalid BusinessID")
		}
		promoCode.BusinessID = businessID
	}
	if err := common.DB.Save(promoCode).Scan(&promoCode).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to update PromotionCodeID %s", in.GetID()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PromotionCodeChange, promoCode.ID.String()))
	return promoCode, nil
}

//DeletePromoCode by ID
func DeletePromoCode(id uuid.UUID) (bool, error) {
	promoCode, err := getPromotionCodeByID(id)
	if err != nil {
		return false, err
	}
	if err := common.DB.Delete(promoCode).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, fmt.Sprintf("Failed to update PromotionCodeID %s", id.String()))
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PromotionCodeChange, promoCode.ID.String()))
	return true, nil
}

//UnaryServerAuthInterceptor user unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx, err := common.Authenticate(ctx)
		if err != nil {
			return nil, err
		}
		return handler(newCtx, req)
	}
}

//StreamServerAuthInterceptor ShoppingCart stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx, err := common.Authenticate(stream.Context())
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

func checkPromoCodes(shoppingCart *entity.ShoppingCart) (*entity.ShoppingCart, []string) {
	var newPromoList []*entity.PromotionCode
	var errorList []string
	for _, p := range shoppingCart.PromotionCodes {
		promoCode, err := getPromotionCodeByID(p.ID)
		if err == nil {
			if time.Now().After(promoCode.ActiveFrom) && time.Now().Before(promoCode.ActiveTo) && promoCode.IsActive {
				newPromoList = append(newPromoList, promoCode)
			} else {
				errorList = append(errorList, fmt.Sprintf("PromoCode: %s is no longer available", p.Code))
				logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.PromotionCodeChange, promoCode.ID.String()))
			}
		} else {
			if err == status.Error(codes.NotFound, "") {
				errorList = append(errorList, fmt.Sprintf("PromoCode: %s is no longer available", p.Code))
			}
		}
	}
	shoppingCart.PromotionCodes = newPromoList
	return shoppingCart, errorList
}

func getPromotionCodeByID(id uuid.UUID) (*entity.PromotionCode, error) {
	promoCode := &entity.PromotionCode{}
	key := redisClient.RedisMapKeys.GetPromotionCodeKey(id.String())
	err := redisClient.GetRedisValue(key, promoCode)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get PromotionCode ID: %s", id.String()))
	}
	return promoCode, nil
}

func getPromotionCodesByEntityID(entityID uuid.UUID) ([]*entity.PromotionCode, error) {
	var promotionCodes []*entity.PromotionCode
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.PromotionCodeHashMap,
		redisClient.RedisMapKeys.GetPromotionCodesByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return promotionCodes, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get PromotionCodes by EntityID: %s", entityID))
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get PromotionCodes by EntityID: %s", entityID))
	}
	for _, v := range vals {
		if v != nil {
			promotionCode := &entity.PromotionCode{}
			err = promotionCode.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				promotionCodes = append(promotionCodes, promotionCode)
			}
		}
	}
	return promotionCodes, nil
}

func getPromotionCodeByCode(entityID uuid.UUID, code string) (*entity.PromotionCode, error) {
	promoCode := &entity.PromotionCode{}
	mapKey := redisClient.RedisMapKeys.GetPromotionCodeByPromotionCodeMapKey(entityID.String(), code)
	key, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.PromotionCodeHashMap, mapKey)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get PromotionCodes by code: %s", code))
	}
	err = redisClient.GetRedisValue(key, promoCode)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get PromotionCodes by code: %s", code))
	}
	return promoCode, nil
}

func getShoppingCartByID(id uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart := &entity.ShoppingCart{}
	key := redisClient.RedisMapKeys.GetShoppingCartKey(id.String())
	err := redisClient.GetRedisValue(key, shoppingCart)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get ShoppingCart ID: %s", id.String()))
	}
	return shoppingCart, nil
}

func getShoppingCartsByEntity(entityID uuid.UUID) ([]*entity.ShoppingCart, error) {
	var shoppingCarts []*entity.ShoppingCart
	mapVals, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.ShoppingCartHashMap,
		redisClient.RedisMapKeys.GetShoppingCartsByEntityMapKey(entityID.String()))
	if err == redis.Nil {
		return shoppingCarts, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get ShoppingCarts by EntityID: %s", entityID))
	}
	vals, err := redisClient.GetRedisValues(mapVals)
	if err == redis.Nil {
		return shoppingCarts, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get ShoppingCarts by EntityID: %s", entityID))
	}
	for _, v := range vals {
		if v != nil {
			shoppingCart := &entity.ShoppingCart{}
			err = shoppingCart.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				shoppingCarts = append(shoppingCarts, shoppingCart)
			}
		}
	}
	return shoppingCarts, nil
}

func getShoppingCartByEntityAndBusiness(entityID, businessID uuid.UUID) (*entity.ShoppingCart, error) {
	shoppingCart := &entity.ShoppingCart{}
	mapKey := redisClient.RedisMapKeys.GetShoppingCartsBySpecificEntityAndBusinessMapKey(entityID.String(), businessID.String())
	key, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.ShoppingCartHashMap, mapKey)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get ShoppingCart by EntityID: %s and BusinessID: %s", entityID.String(), businessID.String()))
	}
	err = redisClient.GetRedisValue(key, shoppingCart)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("Failed to get ShoppingCart by EntityID: %s and BusinessID: %s", entityID.String(), businessID.String()))
	}
	return shoppingCart, nil
}
