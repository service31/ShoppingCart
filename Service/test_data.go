package service

import (
	"time"

	"github.com/gogo/protobuf/types"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redis "gitlab.com/service31/Common/Redis"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/ShoppingCart"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redis.RedisClient.Close()
}

//GetNewShoppingCartTestData test data
func GetNewShoppingCartTestData(entityID uuid.UUID) []*module.CreateShoppingCartRequest {
	var requests []*module.CreateShoppingCartRequest
	for i := 0; i < 5; i++ {
		requests = append(requests, &module.CreateShoppingCartRequest{
			EntityID:   entityID.String(),
			BusinessID: uuid.NewV4().String(),
			ShippingAddress: &module.AddressDTO{
				AddressID:   uuid.NewV4().String(),
				Prefix:      common.GetRandomString(5),
				Firstname:   common.GetRandomString(10),
				Lastname:    common.GetRandomString(10),
				IsBusiness:  false,
				Address1:    common.GetRandomString(15),
				Address2:    common.GetRandomString(15),
				PhoneNumber: "+1" + common.GetRandomNumberString(10),
				Zip:         common.GetRandomNumberString(5),
				City:        common.GetRandomString(5),
				State:       common.GetRandomString(10),
				Tax:         0,
			},
			BillingAddress: &module.AddressDTO{
				AddressID:   uuid.NewV4().String(),
				Prefix:      common.GetRandomString(5),
				Firstname:   common.GetRandomString(10),
				Lastname:    common.GetRandomString(10),
				IsBusiness:  false,
				Address1:    common.GetRandomString(15),
				Address2:    common.GetRandomString(15),
				PhoneNumber: "+1" + common.GetRandomNumberString(10),
				Zip:         common.GetRandomNumberString(5),
				City:        common.GetRandomString(5),
				State:       common.GetRandomString(10),
				Tax:         0,
			},
			Product: &module.ShoppingCartItemDTO{
				ProductID: uuid.NewV4().String(),
				Quantity:  16,
			},
		})
	}
	return requests
}

//GetTestPromotionCodeData test data
func GetTestPromotionCodeData(entityID uuid.UUID) []*module.CreatePromoCodeRequest {
	var requests []*module.CreatePromoCodeRequest
	for i := 0; i < 5; i++ {
		from := types.TimestampNow()
		to, _ := types.TimestampProto(time.Now().Add(1 * time.Hour))
		requests = append(requests, &module.CreatePromoCodeRequest{
			EntityID:           entityID.String(),
			Code:               common.GetRandomString(5),
			IsUSD:              true,
			IsPercent:          false,
			DiscountAmount:     1,
			BusinessID:         uuid.NewV4().String(),
			LimitedAmount:      1,
			IsActive:           true,
			IsNewCustomerOnly:  false,
			IsBusinessSpecific: false,
			ActiveFrom:         from,
			ActiveTo:           to,
		})
	}
	return requests
}

//GetUpdatePromotionCodeData test data
func GetUpdatePromotionCodeData(id uuid.UUID) *module.UpdatePromoCodeRequest {
	from := types.TimestampNow()
	to, _ := types.TimestampProto(time.Now().Add(1 * time.Hour))
	return &module.UpdatePromoCodeRequest{
		ID:                 id.String(),
		Code:               common.GetRandomString(10),
		IsUSD:              true,
		IsPercent:          false,
		DiscountAmount:     1,
		BusinessID:         uuid.NewV4().String(),
		LimitedAmount:      1,
		IsActive:           true,
		IsNewCustomerOnly:  false,
		IsBusinessSpecific: false,
		ActiveFrom:         from,
		ActiveTo:           to,
	}
}

//GetAddressTestData test data
func GetAddressTestData(id uuid.UUID) *module.UpdateAddressInShoppingCartRequest {
	return &module.UpdateAddressInShoppingCartRequest{
		ID:          id.String(),
		AddressID:   uuid.NewV4().String(),
		Prefix:      common.GetRandomString(5),
		Firstname:   common.GetRandomString(10),
		Lastname:    common.GetRandomString(10),
		IsBusiness:  false,
		Address1:    common.GetRandomString(15),
		Address2:    common.GetRandomString(15),
		PhoneNumber: "+1" + common.GetRandomNumberString(10),
		Zip:         common.GetRandomNumberString(5),
		City:        common.GetRandomString(5),
		State:       common.GetRandomString(10),
		Tax:         0,
	}

}

//SetupShoppingCart redis
func SetupShoppingCart(shoppingCart *entity.ShoppingCart) {
	key := redisClient.RedisMapKeys.GetShoppingCartKey(shoppingCart.ID.String())
	shoppingCartBusinessMapKey := redisClient.RedisMapKeys.GetShoppingCartMapKey(shoppingCart.BusinessID.String(), shoppingCart.ID.String())
	shoppingCartUserMapKey := redisClient.RedisMapKeys.GetShoppingCartMapKey(shoppingCart.EntityID.String(), shoppingCart.ID.String())
	shoppingCartByEntityAndBusinessMapKey := redisClient.RedisMapKeys.GetShoppingCartsBySpecificEntityAndBusinessMapKey(shoppingCart.EntityID.String(), shoppingCart.BusinessID.String())
	bytes, err := shoppingCart.MarshalBinary()
	logger.CheckFatal(err)
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartBusinessMapKey, key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartUserMapKey, key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.ShoppingCartHashMap, shoppingCartByEntityAndBusinessMapKey, key)
}

//SetupPromotionCode redis
func SetupPromotionCode(promotionCode *entity.PromotionCode) {
	key := redisClient.RedisMapKeys.GetPromotionCodeKey(promotionCode.ID.String())
	promotionCodeMapKey := redisClient.RedisMapKeys.GetPromotionCodeMapKey(promotionCode.EntityID.String(), promotionCode.ID.String())
	promotionCodeCodeMapKey := redisClient.RedisMapKeys.GetPromotionCodeByPromotionCodeMapKey(promotionCode.EntityID.String(), promotionCode.Code)
	bytes, err := promotionCode.MarshalBinary()
	logger.CheckFatal(err)
	redisClient.SetRedisValue(key, bytes)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.PromotionCodeHashMap, promotionCodeMapKey, key)
	redisClient.SetRedisHashMapValue(redisClient.RedisMapKeys.PromotionCodeHashMap, promotionCodeCodeMapKey, key)
}
