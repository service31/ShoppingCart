package service

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/ShoppingCart"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.PromotionCode{}, &entity.ShoppingCart{}, &entity.ShoppingCartItem{}, &entity.Address{})
	defer DoneTesting()
	m.Run()
}

func TestCreateShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)

	shoppingCart, err := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])

	if err != nil {
		t.Error(err)
	} else if shoppingCart.ShippingAddress.Address1 != createIns[0].GetShippingAddress().GetAddress1() {
		t.Error("Address1 doesn't match")
	}
}

func TestGetShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	for _, in := range createIns {
		shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(in.BusinessID), in)
		SetupShoppingCart(shoppingCart)
	}

	shoppingCart, err := GetShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID))

	if err != nil {
		t.Error(err)
	} else if shoppingCart.ShippingAddress.Address1 != createIns[0].GetShippingAddress().GetAddress1() {
		t.Error("Address1 doesn't match")
	}
}

func TestGetShoppingCartWithoutShoppingCart(t *testing.T) {
	_, err := GetShoppingCart(uuid.NewV4(), uuid.NewV4())
	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetShoppingCartByID(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := GetShoppingCartByID(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if shoppingCart.ShippingAddress.Address1 != createIns[0].GetShippingAddress().GetAddress1() {
		t.Error("Address1 doesn't match")
	}
}

func TestGetShoppingCartByIDWithoutShoppingCart(t *testing.T) {
	_, err := GetShoppingCartByID(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestAddProductInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := AddProductInShoppingCart(shoppingCart.ID, uuid.NewV4(), 60)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.ShoppingCartItems) != 2 {
		t.Error("Product didn't add to shopping cart")
	}
}

func TestAddProductInShoppingCartWithoutShoppingCart(t *testing.T) {
	_, err := AddProductInShoppingCart(uuid.NewV4(), uuid.NewV4(), 60)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestRemoveProductInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := RemoveProductInShoppingCart(shoppingCart.ID, shoppingCart.ShoppingCartItems[0].ProductID)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.ShoppingCartItems) != 0 {
		t.Error("Product didn't remove from shopping cart")
	}
}

func TestRemoveProductInShoppingCartWithoutShoppingCart(t *testing.T) {
	_, err := RemoveProductInShoppingCart(uuid.NewV4(), uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestRemoveProductInShoppingCartWithoutProduct(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := RemoveProductInShoppingCart(shoppingCart.ID, uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestAddPromoCodeInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	promoIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, promoIns[0])
	SetupPromotionCode(promo)

	shoppingCart, err := AddPromoCodeInShoppingCart(shoppingCart.ID, promo.ID)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.PromotionCodes) < 1 {
		t.Error("Failed to add promotion code")
	}
}

func TestAddPromoCodeInShoppingCartWithoutShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	promoIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, promoIns[0])
	SetupPromotionCode(promo)

	_, err := AddPromoCodeInShoppingCart(uuid.NewV4(), promo.ID)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestAddPromoCodeInShoppingCartWithoutPromoCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := AddPromoCodeInShoppingCart(shoppingCart.ID, uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestValidatePromoCodesInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	promoIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, promoIns[0])
	SetupPromotionCode(promo)
	shoppingCart, _ = AddPromoCodeInShoppingCart(shoppingCart.ID, promo.ID)
	SetupShoppingCart(shoppingCart)

	shoppingCart, _, err := ValidatePromoCodesInShoppingCart(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.PromotionCodes) < 1 {
		t.Error("PromoCode removed but shouldn't be")
	}
}

func TestValidatePromoCodesInShoppingCartWithExpiredPromo(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	promoIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, promoIns[0])
	SetupPromotionCode(promo)
	shoppingCart, _ = AddPromoCodeInShoppingCart(shoppingCart.ID, promo.ID)
	SetupShoppingCart(shoppingCart)
	promo.ActiveTo = promo.ActiveFrom
	SetupPromotionCode(promo)

	shoppingCart, errs, err := ValidatePromoCodesInShoppingCart(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.PromotionCodes) != 0 {
		t.Error("Expired PromoCode is not removed")
	} else if len(errs) < 1 {
		t.Error("No promo removed message returned")
	}
}

func TestValidatePromoCodesInShoppingCartWithoutShopingCart(t *testing.T) {
	_, _, err := ValidatePromoCodesInShoppingCart(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestValidatePromoCodesInShoppingCartWithoutPromotionCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, errs, err := ValidatePromoCodesInShoppingCart(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if len(errs) > 0 {
		t.Errorf("Validation errors returned but shouldn't be %v", errs)
	}
}

func TestRemovePromoCodeInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	promoIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, promoIns[0])
	SetupPromotionCode(promo)
	shoppingCart, _ = AddPromoCodeInShoppingCart(shoppingCart.ID, promo.ID)
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := RemovePromoCodeInShoppingCart(shoppingCart.ID, promo.ID)

	if err != nil {
		t.Error(err)
	} else if len(shoppingCart.PromotionCodes) > 0 {
		t.Error("PromoCode is not removed")
	}
}

func TestRemovePromoCodeInShoppingCartWithoutPromoCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	_, err := RemovePromoCodeInShoppingCart(shoppingCart.ID, uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestRemovePromoCodeInShoppingCartWithoutShoppingCart(t *testing.T) {
	_, err := RemovePromoCodeInShoppingCart(uuid.NewV4(), uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateShippingAddressInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	updateIn := GetAddressTestData(shoppingCart.ID)

	shoppingCart, err := UpdateShippingAddressInShoppingCart(shoppingCart.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if shoppingCart.ShippingAddress.Address1 != updateIn.GetAddress1() {
		t.Error("Address1 doesn't match")
	}
}

func TestUpdateShippingAddressInShoppingCartWithoutShoppingCart(t *testing.T) {
	updateIn := GetAddressTestData(uuid.NewV4())

	_, err := UpdateShippingAddressInShoppingCart(uuid.NewV4(), updateIn)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateBillingAddressInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)
	updateIn := GetAddressTestData(shoppingCart.ID)

	shoppingCart, err := UpdateBillingAddressInShoppingCart(shoppingCart.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if shoppingCart.BillingAddress.Address1 != updateIn.GetAddress1() {
		t.Error("Address1 doesn't match")
	}
}

func TestUpdateBillingAddressInShoppingCartWithoutShoppingCart(t *testing.T) {
	updateIn := GetAddressTestData(uuid.NewV4())

	_, err := UpdateBillingAddressInShoppingCart(uuid.NewV4(), updateIn)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdateShippingMethodInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	smID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := UpdateShippingMethodInShoppingCart(shoppingCart.ID, smID)

	if err != nil {
		t.Error(err)
	} else if shoppingCart.ShippingMethodID != smID {
		t.Error("ShippingMethod doesn't match")
	}
}

func TestUpdateShippingMethodInShoppingCartWithoutShippingMethod(t *testing.T) {
	_, err := UpdateShippingMethodInShoppingCart(uuid.NewV4(), uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdatePointsRedeemInShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	shoppingCart, err := UpdatePointsRedeemInShoppingCart(shoppingCart.ID, 60)

	if err != nil {
		t.Error(err)
	} else if shoppingCart.PointsRedeem != 60 {
		t.Error("PointsRedeem doesn't match")
	}
}

func TestUpdatePointsRedeemInShoppingCartWithoutShoppingCart(t *testing.T) {
	_, err := UpdatePointsRedeemInShoppingCart(uuid.NewV4(), 60)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestCompleteShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	response, err := CompleteShoppingCart(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if !response {
		t.Error("ShoppingCart not completed")
	}
}

func TestCompleteShoppingCartWithoutShoppingCart(t *testing.T) {
	response, err := CompleteShoppingCart(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	} else if response {
		t.Error("Complete is true but it shouldn't be")
	}
}

func TestMergeShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	from, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(from)
	to, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[1].BusinessID), createIns[1])
	SetupShoppingCart(to)

	merged, err := MergeShoppingCart(from.ID, to.ID)

	if err != nil {
		t.Error(err)
	} else if len(merged.ShoppingCartItems) != 2 {
		t.Error("ShoppingCartItems are not merged")
	}
}

func TestMergeShoppingCartWithoutFrom(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	to, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[1].BusinessID), createIns[1])
	SetupShoppingCart(to)

	_, err := MergeShoppingCart(uuid.NewV4(), to.ID)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestMergeShoppingCartWithoutTo(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	from, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(from)

	_, err := MergeShoppingCart(from.ID, uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestMergeShoppingCartWithoutSame(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	from, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(from)

	merged, err := MergeShoppingCart(from.ID, from.ID)

	if err != nil {
		t.Error(err)
	} else if len(merged.ShoppingCartItems) != 1 {
		t.Error("ShoppingCartItems are merged with same cart")
	}
}

func TestDeleteShoppingCart(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetNewShoppingCartTestData(entityID)
	shoppingCart, _ := CreateShoppingCart(entityID, uuid.FromStringOrNil(createIns[0].BusinessID), createIns[0])
	SetupShoppingCart(shoppingCart)

	response, err := DeleteShoppingCart(shoppingCart.ID)

	if err != nil {
		t.Error(err)
	} else if !response {
		t.Error("Response is false")
	}
}

func TestDeleteShoppingCartWihtouShoppingCart(t *testing.T) {
	response, err := DeleteShoppingCart(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	} else if response {
		t.Error("Response is true")
	}
}

func TestCreatePromoCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)

	promo, err := CreatePromoCode(entityID, createIns[0])

	if err != nil {
		t.Error(err)
	} else if promo.Code != createIns[0].GetCode() {
		t.Error("Code doesn't match")
	}
}

func TestGetAllPromoCodesByEntity(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)
	for _, in := range createIns {
		promo, _ := CreatePromoCode(entityID, in)
		SetupPromotionCode(promo)
	}

	promos, err := GetAllPromoCodesByEntity(entityID)

	if err != nil {
		t.Error(err)
	} else if len(promos) != len(createIns) {
		t.Errorf("Created %d but got %d", len(createIns), len(promos))
	}
}

func TestGetAllPromoCodesByEntityWithoutPromoCodes(t *testing.T) {
	_, err := GetAllPromoCodesByEntity(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetPromoCodeByID(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, createIns[0])
	SetupPromotionCode(promo)

	promo, err := GetPromoCodeByID(promo.ID)

	if err != nil {
		t.Error(err)
	} else if promo.Code != createIns[0].GetCode() {
		t.Error("Code doesn't match")
	}
}

func TestGetPromoCodeByIDWithoutPromoCode(t *testing.T) {
	_, err := GetPromoCodeByID(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	}
}

func TestGetPromoCodeByCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, createIns[0])
	SetupPromotionCode(promo)

	promo, err := GetPromoCodeByCode(entityID, promo.Code)

	if err != nil {
		t.Error(err)
	} else if promo.Code != createIns[0].GetCode() {
		t.Error("Code doesn't match")
	}
}

func TestGetPromoCodeByCodeWithoutPromoCode(t *testing.T) {
	_, err := GetPromoCodeByCode(uuid.NewV4(), common.GetRandomString(10))

	if err == nil {
		t.Error("No error returned")
	}
}

func TestUpdatePromoCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, createIns[0])
	SetupPromotionCode(promo)
	updateIn := GetUpdatePromotionCodeData(promo.ID)

	promo, err := UpdatePromoCode(promo.ID, updateIn)

	if err != nil {
		t.Error(err)
	} else if promo.Code != updateIn.GetCode() {
		t.Error("Code doesn't match")
	}
}

func TestUpdatePromoCodeWithoutPromoCode(t *testing.T) {
	updateIn := GetUpdatePromotionCodeData(uuid.NewV4())

	_, err := UpdatePromoCode(uuid.NewV4(), updateIn)

	if err == nil {
		t.Error("No error returned")
	}
}

func TestDeletePromoCode(t *testing.T) {
	entityID := uuid.NewV4()
	createIns := GetTestPromotionCodeData(entityID)
	promo, _ := CreatePromoCode(entityID, createIns[0])
	SetupPromotionCode(promo)

	response, err := DeletePromoCode(promo.ID)

	if err != nil {
		t.Error(err)
	} else if !response {
		t.Error("Response is false")
	}
}

func TestDeletePromoCodeWithoutPromoCode(t *testing.T) {
	response, err := DeletePromoCode(uuid.NewV4())

	if err == nil {
		t.Error("No error returned")
	} else if response {
		t.Error("Response is true")
	}
}
