module gitlab.com/service31/ShoppingCart

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gogo/protobuf v1.3.1
	github.com/gogo/status v1.1.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.4
	github.com/satori/go.uuid v1.2.0
	gitlab.com/service31/Common v0.0.0-20200420013348-5f3b6808cd88
	gitlab.com/service31/Data v0.0.0-20200420013210-e43da3937136
	google.golang.org/grpc v1.28.1
)
