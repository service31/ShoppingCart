package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/ShoppingCart"
	module "gitlab.com/service31/Data/Module/ShoppingCart"
	service "gitlab.com/service31/ShoppingCart/Service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedShoppingCartServiceServer
}

func (s *server) CreateShoppingCart(ctx context.Context, in *module.CreateShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	businessID, err := uuid.FromString(in.GetBusinessID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad BusinessID")
	}
	shoppingCart, err := service.CreateShoppingCart(entityID, businessID, in)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetShoppingCart(ctx context.Context, in *module.GetShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	businessID, err := uuid.FromString(in.GetBusinessID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad BusinessID")
	}
	shoppingCart, err := service.GetShoppingCart(entityID, businessID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, fmt.Sprintf("ShoppingCart data has error ID: %s. ErrorMessage: %s", shoppingCart.ID.String(), err.Error()))
	}
	return dto, nil
}

func (s *server) GetShoppingCartByID(ctx context.Context, in *module.GetShoppingCartByIDRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shoppingCart, err := service.GetShoppingCartByID(ID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) AddProductInShoppingCart(ctx context.Context, in *module.AddProductInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	productID, err := uuid.FromString(in.GetProduct().GetProductID())
	if err != nil || productID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}
	shoppingCart, err := service.AddProductInShoppingCart(ID, productID, in.GetProduct().GetQuantity())
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) RemoveProductInShoppingCart(ctx context.Context, in *module.RemoveProductInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	productID, err := uuid.FromString(in.GetProductID())
	if err != nil || productID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ProductID")
	}
	shoppingCart, err := service.RemoveProductInShoppingCart(ID, productID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) ValidatePromoCodesInShoppingCart(ctx context.Context, in *module.ValidatePromoCodesInShoppingCartRequest) (*module.ValidatePromoCodesInShoppingCartResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shoppingCart, errs, err := service.ValidatePromoCodesInShoppingCart(ID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &module.ValidatePromoCodesInShoppingCartResponse{ShoppingCart: dto, ErrorMessages: errs}, nil
}

func (s *server) AddPromoCodeInShoppingCart(ctx context.Context, in *module.AddPromoCodeInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	promoCodeID, err := uuid.FromString(in.GetPromoCodeID())
	if err != nil || promoCodeID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad PromoCodeID")
	}
	shoppingCart, err := service.AddPromoCodeInShoppingCart(ID, promoCodeID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) RemovePromoCodeInShoppingCart(ctx context.Context, in *module.RemovePromoCodeInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	promoCodeID, err := uuid.FromString(in.GetPromoCodeID())
	if err != nil || promoCodeID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad PromoCodeID")
	}
	shoppingCart, err := service.RemovePromoCodeInShoppingCart(ID, promoCodeID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdateShippingAddressInShoppingCart(ctx context.Context, in *module.UpdateAddressInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shoppingCart, err := service.UpdateShippingAddressInShoppingCart(ID, in)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdateBillingAddressInShoppingCart(ctx context.Context, in *module.UpdateAddressInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shoppingCart, err := service.UpdateBillingAddressInShoppingCart(ID, in)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdateShippingMethodInShoppingCart(ctx context.Context, in *module.UpdateShippingMethodInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shippingMethodID, err := uuid.FromString(in.GetShippingMethodID())
	if err != nil || shippingMethodID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ShipingMethodID")
	}
	shoppingCart, err := service.UpdateShippingMethodInShoppingCart(ID, shippingMethodID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdatePointsRedeemInShoppingCart(ctx context.Context, in *module.UpdatePointsRedeemInShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	shoppingCart, err := service.UpdatePointsRedeemInShoppingCart(ID, in.GetPointsRedeem())
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) CompleteShoppingCart(ctx context.Context, in *module.CompleteShoppingCartReuqest) (*module.BoolResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.CompleteShoppingCart(ID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func (s *server) MergeShoppingCart(ctx context.Context, in *module.MergeShoppingCartRequest) (*module.DetailedShoppingCartDTO, error) {
	fromID, err := uuid.FromString(in.GetFromID())
	if err != nil || fromID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad FromID")
	}
	toID, err := uuid.FromString(in.GetToID())
	if err != nil || toID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ToID")
	}
	shoppingCart, err := service.MergeShoppingCart(fromID, toID)
	if err != nil {
		return nil, err
	}
	dto, err := shoppingCart.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) DeleteShoppingCart(ctx context.Context, in *module.DeleteShoppingCartRequest) (*module.BoolResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.DeleteShoppingCart(ID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func (s *server) CreatePromoCode(ctx context.Context, in *module.CreatePromoCodeRequest) (*module.DetailedPromotionCodeDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	promoCode, err := service.CreatePromoCode(entityID, in)
	if err != nil {
		return nil, err
	}
	dto, err := promoCode.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetAllPromoCodesByEntity(ctx context.Context, in *module.GetAllPromoCodesByEntityRequest) (*module.RepeatedDetailedPromotionCodeDTO, error) {
	var dtos []*module.DetailedPromotionCodeDTO
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	promoCodes, err := service.GetAllPromoCodesByEntity(entityID)
	if err != nil {
		return nil, err
	}
	for _, p := range promoCodes {
		dto, err := p.ToDTO()
		if logger.CheckError(err) {
			return nil, status.Error(codes.Internal, fmt.Sprintf("PromoCodeID: %s has error. Error Message: %s", p.ID.String(), err.Error()))
		}
		dtos = append(dtos, dto)
	}
	return &module.RepeatedDetailedPromotionCodeDTO{Results: dtos}, nil
}

func (s *server) GetPromoCodeByID(ctx context.Context, in *module.GetPromoCodeByIDRequest) (*module.DetailedPromotionCodeDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	promoCode, err := service.GetPromoCodeByID(ID)
	if err != nil {
		return nil, err
	}
	dto, err := promoCode.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) GetPromoCodeByCode(ctx context.Context, in *module.GetPromoCodeByCodeRequest) (*module.DetailedPromotionCodeDTO, error) {
	entityID, err := uuid.FromString(in.GetEntityID())
	if err != nil || entityID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad EntityID")
	}
	promoCode, err := service.GetPromoCodeByCode(entityID, in.GetPromoCode())
	if err != nil {
		return nil, err
	}
	dto, err := promoCode.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) UpdatePromoCode(ctx context.Context, in *module.UpdatePromoCodeRequest) (*module.DetailedPromotionCodeDTO, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	promoCode, err := service.UpdatePromoCode(ID, in)
	if err != nil {
		return nil, err
	}
	dto, err := promoCode.ToDTO()
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return dto, nil
}

func (s *server) DeletePromoCode(ctx context.Context, in *module.DeletePromoCodeRequest) (*module.BoolResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil || ID == uuid.Nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.DeletePromoCode(ID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterShoppingCartServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/shopping_cart.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterShoppingCartServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.PromotionCode{}, &entity.ShoppingCart{}, &entity.ShoppingCartItem{}, &entity.Address{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
